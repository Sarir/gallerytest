<?php
/**
 * Created by PhpStorm.
 * User: sarir
 * Date: 12.06.2018
 * Time: 18:34
 */

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'title', 'album_id', 'file_name'
    ];

    protected $guarded = [
        'id'
    ];

    protected $changes = [
        'title', 'album_id'
    ];
}