<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

abstract class AbstractModelController extends Controller
{
    protected $repo;
    protected $validator;
    protected $resource;

    public function __construct(RepositoryInterface $repository, LaravelValidator $validator, string $resource)
    {
        $this->repo = $repository;
        $this->validator = $validator;
        $this->resource = $resource;
    }

    public function index()
    {
        $models = $this->repo->paginate(5);
        return $this->resource::collection($models);
    }


    public function store(Request $request)
    {
        $input = $request->all();

        $valid = $this->validateCreate($input);

        if (is_bool($valid)) {
            $album = $this->repo->create($input);

            return new $this->resource($album);
        } else {
            return $valid;
        }
    }

    public function show($id)
    {
        $model = $this->findById($id);
        return $model ? new $this->resource($model) : $this->failWithNotFound();
    }

    public function update(Request $request, $id)
    {
        $model = $this->findById($id);

        if ($model) {
            $input = $request->all();

            $valid = $this->validateUpdate($input);

            if (is_bool($valid)) {
                $model = $this->repo->update($input, $id);

                return new $this->resource($model);
            } else {
                return $valid;
            }
        }

        return $model ? $this->store($request) : $this->failWithNotFound();
    }

    public function destroy($id)
    {
        $model = $this->findById($id);

        if ($model) {
            if ($this->repo->delete($id)) {
                return $this->sendSuccess();
            }
        }

        return $this->failWithNotFound();
    }

    /**
     * @param array $input
     * @param string $type
     * @return bool|\Illuminate\Http\JsonResponse
     */
    protected function validateType($input, $type)
    {
        $v = $this->validator;

        $pass = $v->with($input)->passes($type);

        if ($pass) {
            return true;
        } else {
            return $this->failWith($v->errors());
        }
    }

    /**
     * @param array $input
     * @return bool|\Illuminate\Http\JsonResponse
     */
    protected function validateCreate($input)
    {
        return $this->validateType($input, ValidatorInterface::RULE_CREATE);
    }

    /**
     * @param array $input
     * @return bool|\Illuminate\Http\JsonResponse
     */
    protected function validateUpdate($input)
    {
        return $this->validateType($input, ValidatorInterface::RULE_UPDATE);
    }

    /**
     * @param $message array | string
     * @return \Illuminate\Http\JsonResponse
     */
    protected function failWith($message)
    {
        return response()->json([
            'error' => 'true',
            'messages' => $message
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    protected function failWithNotFound()
    {
        return $this->failWith('Not found');
    }

    protected function sendSuccess()
    {
        return response()->json([
            'error' => 'false'
        ]);
    }

    /**
     * @param $id integer
     * @return bool| Model
     */
    protected function findById($id)
    {
        $item = $this->repo->findByField('id', $id);
        return $item->count() == 0 ? false : $item->get(0);
    }
}
