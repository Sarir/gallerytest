<?php
/**
 * Created by PhpStorm.
 * User: sarir
 * Date: 12.06.2018
 * Time: 17:16
 */

namespace App\Http\Controllers;


use App\Http\Resources\Album as AlbumResource;
use App\Http\Resources\Photo;
use App\Repositories\Interfaces\AlbumRepositoryInterface;
use App\Validators\AlbumValidator;
use App\Validators\PhotoValidator;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AlbumController extends AbstractModelController
{
    public function __construct(AlbumRepositoryInterface $repository, AlbumValidator $validator)
    {
        parent::__construct($repository, $validator, AlbumResource::class);
    }

    public function showPreview(Request $req, $id)
    {
        $album = $this->findById($id);

        if (!$album) {
            $this->failWithNotFound();
        }

        $collection = $album->photos;

        if ($collection->count() === 0) {
            try {
                return Storage::disk('public')->get('no_photo.png');
            } catch (FileNotFoundException $e) {
                return redirect('http://placehold.it/300');
            }
        }

        $res = new Photo($collection->random());

        $url = $res->toArray($req)['cached_url'];

        return redirect($url);
    }
}