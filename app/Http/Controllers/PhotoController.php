<?php
/**
 * Created by PhpStorm.
 * User: sarir
 * Date: 12.06.2018
 * Time: 18:45
 */

namespace App\Http\Controllers;


use App\Http\Resources\Photo as PhotoResource;
use App\Repositories\Interfaces\PhotoRepositoryInterface;
use App\Validators\PhotoValidator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PhotoController extends AbstractModelController
{

    public static $PREVIEW_WIDTH = 1200;

    public static $FOLDER_PHOTOS = 'photos';
    public static $FOLDER_ORIGINAL = 'original';
    public static $FOLDER_PREVIEW = 'preview';
    public static $FOLDER_CACHED = 'cached';

    public function __construct(PhotoRepositoryInterface $repository, PhotoValidator $validator)
    {
        parent::__construct($repository, $validator, PhotoResource::class);
    }

    public function showOriginal($id)
    {
        return $this->showImage($id, static::$FOLDER_ORIGINAL);
    }

    public function showPreview($id)
    {
        return $this->showImage($id, static::$FOLDER_PREVIEW);
    }

    public function showCached($id)
    {
        return $this->showImage($id, static::$FOLDER_CACHED);
    }

     private function showImage($id, $type)
     {
         $item = $this->findById($id);

         if (!$item) {
             $this->failWithNotFound();
         }

         $fileName = $item->getAttribute('file_name');

         return Image::make($this->getFullPath($this->getImagePath($type, $fileName)))->response();
     }

    public function store(Request $request)
    {
        // Get file from request
        $image = $request->file('image');

		if ($image == null) {
            return $this->failWith('Image can\'t be null');
        }
		
        if ($image->getError() > 0) {
            return $this->failWith($image->getErrorMessage());
        }

        $mimeType = $image->getMimeType();

        if (!$this->isImage($mimeType)) {
            return $this->failWith('The file must be a picture');
        }

        // If directory or directories doesn't exist create them
        $this->workWithDirectory();

        $extension = explode('/', $mimeType)[1];

        $fileName = $this->imageProcessing($image, $extension);

        // Put file name to request
        $input = $request->all();
        $input['file_name'] = $fileName;
        $request->replace($input);

        return parent::store($request);
    }

    public function destroy($id)
    {
        $item = $this->findById($id);

        if (!$item) {
            return $this->failWithNotFound();
        }

        $fileName = $item->getAttribute('file_name');

        $this->deleteImages($fileName);

        return parent::destroy($id);
    }

    /**
     *  Return file name
     * @param $image UploadedFile
     * @param $extension string
     * @return string
     */
    private function imageProcessing($image, $extension)
    {
        $fileName = md5(date('Y-m-d H:i:s'));

        $imageOriginalPath = $this->getImagePath(static::$FOLDER_ORIGINAL, $fileName, $extension); // Original images
        $imagePreviewPath = $this->getImagePath(static::$FOLDER_PREVIEW, $fileName, $extension); // Cropped images
        $imageCachedPath = $this->getImagePath(static::$FOLDER_CACHED, $fileName, $extension); // minified files

        Storage::disk('local')->put($imageOriginalPath, file_get_contents($image));

        $originalImage = Image::make($this->getFullPath($imageOriginalPath));

		// Widen
        $previewImage = $originalImage->widen(static::$PREVIEW_WIDTH, function ($constraint) {
            $constraint->upsize();
        });

        unset($originalImage);

        $previewImage->save($this->getFullPath($imagePreviewPath));

        $cachedImage = $previewImage->fit(300, 300, function ($constraint) {
            $constraint->upsize();
        });

        unset($previewImage);

        $cachedImage->save($this->getFullPath($imageCachedPath));

        unset($cachedImage);

        return $fileName . '.' . $extension;
    }

    /**
     * @param $mimeType string
     * @return bool
     */
    private function isImage($mimeType)
    {
        return strpos($mimeType, 'image') !== false;
    }

    private function workWithDirectory()
    {
        $disk = Storage::disk('local');

        if (!$disk->exists('photos')) {
            $disk->makeDirectory('photos');
            $disk->makeDirectory('photos/original');
            $disk->makeDirectory('photos/preview');
            $disk->makeDirectory('photos/cached');
        }

        if (!$disk->exists('photos/original')) {
            $disk->makeDirectory('photos/original');
        }

        if (!$disk->exists('photos/preview')) {
            $disk->makeDirectory('photos/preview');
        }

        if (!$disk->exists('photos/cached')) {
            $disk->makeDirectory('photos/cached');
        }

    }

    private function getImageFolder($folderName)
    {
        return static::$FOLDER_PHOTOS . '/' . $folderName;
    }

    private function getImagePath($folder, $fileName, $extension = '')
    {

        $f = $this->getImageFolder($folder);

        if (empty($extension)) {
            return $f . '/' . $fileName;
        }

        return $f . '/' . $fileName . '.' . $extension;
    }

    private function getFullPath($path)
    {
        return storage_path('app') . '/' . $path;
    }

    private function deleteImages($filename)
    {
        $disk = Storage::disk('local');

        $pathOriginal = $this->getImagePath(static::$FOLDER_ORIGINAL, $filename);
        $pathPreview = $this->getImagePath(static::$FOLDER_PREVIEW, $filename);
        $pathCached = $this->getImagePath(static::$FOLDER_CACHED, $filename);

        $disk->delete($pathOriginal);
        $disk->delete($pathPreview);
        $disk->delete($pathCached);
    }
}