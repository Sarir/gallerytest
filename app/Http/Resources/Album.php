<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Album extends JsonResource
{
    public function toArray($request)
    {
        $array = parent::toArray($request);

        $photosCollection = Photo::collection($this->photos);

        $array['photos'] = $photosCollection;

        $array['preview'] = route('get_album_preview', ['id' => $array['id']]);

        return $array;
    }
}
