<?php
/**
 * Created by PhpStorm.
 * User: sarir
 * Date: 12.06.2018
 * Time: 18:46
 */

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class Photo extends JsonResource
{
    public function toArray($request)
    {
        $array = parent::toArray($request);

        unset($array['file_name']);

        $id = ['id' => $array['id']];

        $array['original_url'] = route('get_original', $id);
        $array['preview_url'] = route('get_preview', $id);
        $array['cached_url'] = route('get_cached', $id);

        return $array;
    }
}