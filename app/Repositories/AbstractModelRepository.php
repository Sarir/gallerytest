<?php
/**
 * Created by PhpStorm.
 * User: sarir
 * Date: 12.06.2018
 * Time: 18:20
 */

namespace App\Repositories;


use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

abstract class AbstractModelRepository extends BaseRepository implements RepositoryInterface
{

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}