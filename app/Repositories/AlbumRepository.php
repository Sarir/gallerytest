<?php
/**
 * Class AlbumRepository.
 *
 * @package namespace App\Repositories;
 */

namespace App\Repositories;

use App\Entities\Album;
use App\Repositories\Interfaces\AlbumRepositoryInterface;


class AlbumRepository extends AbstractModelRepository implements AlbumRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Album::class;
    }
    
}
