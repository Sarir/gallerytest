<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AlbumRepositoryInterface.
 *
 * @package namespace App\Repositories;
 */
interface AlbumRepositoryInterface extends RepositoryInterface
{

}
