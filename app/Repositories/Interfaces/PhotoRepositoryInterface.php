<?php

namespace App\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PhotoRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface PhotoRepositoryInterface extends RepositoryInterface
{

}
