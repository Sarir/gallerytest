<?php
/**
 * Created by PhpStorm.
 * User: sarir
 * Date: 12.06.2018
 * Time: 18:39
 */

namespace App\Repositories;

use App\Entities\Photo;
use App\Repositories\Interfaces\PhotoRepositoryInterface;


class PhotoRepository extends AbstractModelRepository implements PhotoRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Photo::class;
    }
}