<?php
/**
 * Created by PhpStorm.
 * User: sarir
 * Date: 12.06.2018
 * Time: 18:36
 */

namespace App\Validators;


use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class PhotoValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'title'	=> 'required',
            'file_name' => 'required',
            'album_id' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            // Rules not needed, but mustn't delete RULE_UPDATE
        ],
    ];
}