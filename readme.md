https://github.com/andersao/l5-repository - for repositories
http://image.intervention.io/getting_started/installation - image work

#API
1. GET api/albums - list of albums with photos if exist
2. POST api/albums - store album
```
{
    "title": "Some cool title"
}
```
3. GET api/albums/preview/{id} - album preview by id
4. GET api/albums/{album} - get album by id
5. PUT api/albums/{album} - update album
```
{
    "title": "Some new cool title"
}
```
6. DELETE api/albums/{album} - Delete album by id
7. GET api/photos - get photos list
8. POST api/photos - store photo
```
{
    "title": "Photo title",
    "album_id": "2",
    "image": Image through form 
}
```
9. GET api/photos/cached/{id} - get cached image by photo id
10. GET api/photos/original/{id} - get original image by photo id
11. GET api/photos/preview/{id} - get preview image by photo id
12. GET api/photos/{photo} - get photo by id with links
13. PUT api/photos/{photo} - update photo
```
{
    "title": "new photo title",
    "album_id": "2" 
}
```
14. DELETE api/photos/{photo} - Delete photos by id