/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

import Vue from 'vue'
window.Vue = Vue;

import VueRouter from 'vue-router'
import Vuex from 'vuex'
import ElementUI from 'element-ui'

Vue.use(Vuex);
Vue.use(ElementUI);
Vue.use(VueRouter);

const store = new Vuex.Store({
    state: {
        albums: []
    },

    mutations: {
        setAlbums(state, albums) {
            this.albums = albums;
        }
    },

    actions: {
        loadAlbums (state) {
            return new Promise((resolve) => {
                window.axios.get('/api/albums').then((data) => {
                    store.commit('setAlbums', data.data.data);
                    resolve();
                })
            });
        }
    }
});

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: require('./components/HomeComponent')
        },
    ],
});

Vue.component('app-component', require('./components/AppComponent'))

const app = new window.Vue({
    el: '#app',
    router,
    store,
    template: "<app-component/>"
});
