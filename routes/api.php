<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('albums', 'AlbumController');
Route::apiResource('photos', 'PhotoController');

Route::get('photos/original/{id}', 'PhotoController@showOriginal')->name('get_original');
Route::get('photos/preview/{id}', 'PhotoController@showPreview')->name('get_preview');
Route::get('photos/cached/{id}', 'PhotoController@showCached')->name('get_cached');

Route::get('albums/preview/{id}', 'AlbumController@showPreview')->name('get_album_preview');